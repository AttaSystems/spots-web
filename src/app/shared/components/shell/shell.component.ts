import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../core/services/api/auth.service';

@Component({
  selector: 'spt-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  constructor(public authService: AuthService) {
  }

  ngOnInit() {
  }

}
