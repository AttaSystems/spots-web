import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'spt-action-button',
  templateUrl: './action-button.component.html',
  styleUrls: ['./action-button.component.scss']
})
export class ActionButtonComponent {

  @Input()
  variety: 'basic' | 'raised' = 'basic';

  @Input()
  type: string;

  @Input()
  matIcon: string;

  @Input()
  faIcon: string;

  @Input()
  text: string;

  @Input()
  color: string;

  @Input()
  loading: boolean;

  @Input()
  disabled: boolean;

  @Output()
  clickEmitter: EventEmitter<any> = new EventEmitter();

  constructor() {

  }
}
