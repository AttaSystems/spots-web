import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material';

import {ShellComponent} from './components/shell/shell.component';
import {ActionButtonComponent} from './components/action-button/action-button.component';

const SHARED_MATERIAL_MODULES = [
  MatToolbarModule,
  MatCardModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule
];

const SHARED_COMPONENTS = [
  ShellComponent,
  ActionButtonComponent
];

@NgModule({
  declarations: [
    ...SHARED_COMPONENTS
  ],
  imports: [
    CommonModule,
    RouterModule,
    ...SHARED_MATERIAL_MODULES
  ],
  exports: [
    CommonModule,
    ...SHARED_MATERIAL_MODULES,
    ...SHARED_COMPONENTS
  ]
})
export class SharedModule {
}
