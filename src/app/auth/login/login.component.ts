import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../core/services/api/auth.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'spt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public userForm: FormGroup;
  public emailControl: AbstractControl;
  public passwordControl: AbstractControl;

  public loginLoading = false;
  public registerLoading = false;

  constructor(private fb: FormBuilder,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.userForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.required],
    });

    this.emailControl = this.userForm.get('email');
    this.passwordControl = this.userForm.get('password');
  }

  doLogin() {
    if (this.userForm.valid) {
      const {email, password} = this.userForm.value;
      this.loginLoading = true;

      this.authService.login(email, password)
        .pipe(
          finalize(() => {
            this.loginLoading = false;
          })
        )
        .subscribe();
    }
  }

  doRegister() {
    if (this.userForm.valid) {
      const {email, password} = this.userForm.value;
      this.registerLoading = true;

      this.authService.register(email, password)
        .pipe(
          finalize(() => {
            this.registerLoading = false;
          })
        )
        .subscribe();
    }
  }
}
