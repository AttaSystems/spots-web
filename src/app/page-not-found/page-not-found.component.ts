import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'spt-page-not-found',
  template: `
      <div class="page-not-found">
          <h1>404 ERROR</h1>
          <p>This is not the web page you are looking for.</p>
          <button mat-raised-button color="primary" [routerLink]="['/']">Take me home!</button>
      </div>
  `,
  styles: [
    ':host { display: flex; flex-direction: column; flex: 1; }',
    '.page-not-found { display: flex; flex-direction: column; flex: 1; justify-content: center; align-items: center; background-color: #F4F4F4 }',
    '.mat-card { width: 400px; height: 400px }'
  ]
})
export class PageNotFoundComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
