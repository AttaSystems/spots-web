export class MapCoordinates {
  constructor(
    public latitude: number,
    public longitude: number
  ) {
  }
}
