import {FactoryProvider, InjectionToken, PLATFORM_ID} from '@angular/core';
import {isPlatformBrowser} from '@angular/common';

const mockStorage: Storage = {
  length: 0,
  clear: () => {
  },
  getItem: () => null,
  key: () => null,
  removeItem: () => {
  },
  setItem: () => {
  }
};

const localStorageFactory = (platformId: object): Storage => {
  if (isPlatformBrowser(platformId)) {
    return localStorage;
  } else {
    return mockStorage;
  }
};

export const LOCAL_STORAGE = new InjectionToken('LocalStorageToken');

export const localStorageProvider: FactoryProvider = {
  provide: LOCAL_STORAGE,
  useFactory: localStorageFactory,
  deps: [PLATFORM_ID]
};

export const LOCAL_STORAGE_PROVIDERS = [
  localStorageProvider
];
