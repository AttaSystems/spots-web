import {Inject, Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {map, shareReplay, tap} from 'rxjs/operators';
import {User} from '../../models/user.model';
import {LOCAL_STORAGE} from '../application/local-storage.service';
import {Router} from '@angular/router';

export const STORAGE_USER_KEY = 'spt-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private authStateSubject: BehaviorSubject<User> = new BehaviorSubject<User>(null);

  public currentUser$: Observable<User> = this.authStateSubject.asObservable().pipe(
    shareReplay(1)
  );
  public isLoggedIn$: Observable<boolean> = this.currentUser$.pipe(
    map(user => !!(user)),
    shareReplay(1)
  );

  constructor(private router: Router,
              private apiService: ApiService,
              @Inject(LOCAL_STORAGE) private localStorage: Storage) {
    this.validateUserSession();
  }

  private validateUserSession() {
    const storageUser = JSON.parse(this.localStorage.getItem(STORAGE_USER_KEY));
    this.updateUserSession(storageUser);
  }

  private updateUserSession(user: User) {
    this.authStateSubject.next(user);

    if (user == null) {
      this.localStorage.removeItem(STORAGE_USER_KEY);
      this.apiService.removeAuthorizationHeader();
    } else {
      this.localStorage.setItem(STORAGE_USER_KEY, JSON.stringify(user));
      this.apiService.setAuthorizationHeader(user.token);
    }
  }

  public login(email: string, password: string): Observable<User> {
    return this.apiService.postJson<User>(`api-user-log-in`, {email, password})
      .pipe(
        tap((user: User) => {
          this.updateUserSession(user);
          this.router.navigate(['/spots']);
        })
      );
  }

  public register(email: string, password: string) {
    return this.apiService.postJson<User>(`api-user-sign-up`, {email, password})
      .pipe(
        tap((user: User) => {
          this.updateUserSession(user);
          this.router.navigate(['/spots']);
        })
      );
  }

  public logout() {
    this.updateUserSession(null);
    this.router.navigate(['/auth', 'login']);
  }

  public getUserSnapshot() {
    return this.authStateSubject.value;
  }
}
