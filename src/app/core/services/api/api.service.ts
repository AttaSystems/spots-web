import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {ApiResponse} from '../../models/api-response.model';
import {map} from 'rxjs/operators';

export interface HttpOptions {
  headers?: HttpHeaders | {
    [header: string]: string | string[];
  };
  observe?: 'body';
  params?: HttpParams | {
    [param: string]: string | string[];
  };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private readonly baseUrl: string;
  private headers: Map<string, string>;

  public constructor(private httpClient: HttpClient) {
    this.baseUrl = ApiService.withSlashEnding(environment.baseUrl);
    this.headers = new Map<string, string>();
  }

  private static withSlashEnding(url: string): string {
    return url.endsWith('/') ? url : `${url}/`;
  }

  private convertUrl(url: string): string {
    return this.baseUrl + url;
  }

  private processOptions(options: HttpOptions): HttpOptions {
    const {headers = new HttpHeaders()}: HttpOptions = options;
    let httpHeaders = headers as HttpHeaders;

    this.headers.forEach((value, key) => {
      if (!httpHeaders.has(key)) {
        httpHeaders = httpHeaders.set(key, value);
      }
    });
    options.headers = httpHeaders;
    return options;
  }

  public addGlobalHeader(name: string, value: string): void {
    this.headers.set(name, value);
  }

  public removeGlobalHeader(name: string): void {
    this.headers.delete(name);
  }

  public setAuthorizationHeader(token: string): void {
    this.addGlobalHeader('token', token);
  }

  public removeAuthorizationHeader(): void {
    this.removeGlobalHeader('Authorization');
  }

  public getJson<T>(url: string, options: HttpOptions = {}): Observable<T> {
    return this.httpClient.get<ApiResponse<T>>(this.convertUrl(url), this.processOptions(options))
      .pipe(
        map((response: ApiResponse<T>) => response.result)
      );
  }

  public postJson<T>(url: string, body: object, options: HttpOptions = {}): Observable<T> {
    return this.httpClient.post<ApiResponse<T>>(this.convertUrl(url), body, this.processOptions(options))
      .pipe(
        map((response: ApiResponse<T>) => response.result)
      );
  }
}
