import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../services/api/auth.service';
import {map, take, tap} from 'rxjs/operators';

@Injectable()
export class NotAuthenticatedGuard implements CanActivate {
  constructor(private router: Router,
              private authService: AuthService) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.isLoggedIn$.pipe(
      take(1),
      tap(isLoggedIn => {
        if (isLoggedIn) {
          this.router.navigate(['/spots']);
        }
      }),
      map(isLoggedIn => !isLoggedIn)
    );
  }
}
