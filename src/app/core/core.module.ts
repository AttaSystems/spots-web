import {NgModule, Optional, SkipSelf} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {localStorageProvider} from './services/application/local-storage.service';

import {ApiService} from './services/api/api.service';
import {IsAuthenticatedGuard} from './guards/is-authenticated.guard';
import {NotAuthenticatedGuard} from './guards/not-authenticated.guard';

@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  providers: [
    localStorageProvider,
    ApiService,
    IsAuthenticatedGuard,
    NotAuthenticatedGuard
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}
