import {Component, OnInit} from '@angular/core';
import {ApiService} from './core/services/api/api.service';

@Component({
  selector: 'spt-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.apiService.addGlobalHeader('Content-Type', 'application/json');
  }
}
