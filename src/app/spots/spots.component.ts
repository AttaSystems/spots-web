import {Component, OnInit} from '@angular/core';

import {from, Observable} from 'rxjs';
import {take} from 'rxjs/operators';

import * as L from 'leaflet';

import {environment} from '../../environments/environment';
import {MapCoordinates} from '../core/models/map-coordinates.model';

const DEFAULT_COORDS: MapCoordinates = {
  latitude: 44.4268,
  longitude: 26.1025
};

@Component({
  selector: 'spt-spots',
  templateUrl: './spots.component.html',
  styleUrls: ['./spots.component.scss']
})
export class SpotsComponent implements OnInit {

  private mapObject: any;

  constructor() {
  }

  ngOnInit() {
    this.setupMap();
  }

  private setupMap() {
    this.getCurrentPosition()
      .pipe(take(1))
      .subscribe(
        ({latitude, longitude}) => {
          this.mapObject = L.map('map-container').setView([latitude, longitude], 7);

          L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: environment.mapboxAccessToken
          }).addTo(this.mapObject);
        }
      );

  }

  private getCurrentPosition(): Observable<MapCoordinates> {
    const coordsPromise: Promise<MapCoordinates> = new Promise(((resolve, reject) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          position => resolve(position.coords)
        );
      } else {
        resolve(DEFAULT_COORDS);
      }
    }));

    return from(coordsPromise);
  }
}
