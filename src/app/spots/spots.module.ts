import {NgModule} from '@angular/core';

import {SpotsRoutingModule} from './spots-routing.module';
import {SpotsComponent} from './spots.component';
import {SharedModule} from '../shared/shared.module';


@NgModule({
  declarations: [
    SpotsComponent
  ],
  imports: [
    SharedModule,
    SpotsRoutingModule
  ]
})
export class SpotsModule {
}
