import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {IsAuthenticatedGuard} from './core/guards/is-authenticated.guard';
import {NotAuthenticatedGuard} from './core/guards/not-authenticated.guard';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'spots'},
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    canActivate: [NotAuthenticatedGuard]
  },
  {
    path: 'spots',
    loadChildren: () => import('./spots/spots.module').then(m => m.SpotsModule),
    canActivate: [IsAuthenticatedGuard],
  },
  {path: '**', pathMatch: 'full', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
